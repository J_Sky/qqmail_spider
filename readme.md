事前准备：

1.下载并安装python3.x，并能够正常使用

2.下载并安装chrome浏览器，并下载浏览器驱动，详情参考：https://blog.csdn.net/yoyocat915/article/details/80576470

3.pip install selenium

4.windows系统下，C盘下新建文件夹：QQmail

5.qq邮箱开启高级搜索功能

6.运行脚本期间不得登录QQ，否则会出现错误。

运行

1.python -V能够出现版本号即为python安装成功。

![image-20210707090531761](C:\Users\zhangjin\AppData\Roaming\Typora\typora-user-images\image-20210707090531761.png)

2.下载脚本，修改其中参数，修改参数页详情参考代码中注释部分，写得清楚

3.运行：python pa.py

4.全程没有报错，运行完毕，即下载全部完成！