import os
import time


from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
# 1.创建Chrome浏览器对象，这会在电脑上在打开一个浏览器窗口
browser = webdriver.Chrome()
# 2.通过浏览器向服务器发送URL请求
browser.get("https://mail.qq.com/cgi-bin/loginpage")
browser.maximize_window()
# 找到ｉｆｒａｍｅ切换进去
browser.switch_to.frame(browser.find_element_by_id("login_frame"))
# 输入用户名
browser.find_element_by_id('u').send_keys('1296100904@qq.com')
# 输入密码
browser.find_element_by_id('p').send_keys('')
# 点击登录
browser.find_element_by_id('login_button').click()
time.sleep(5)
# 进入高级搜索
browser.find_element_by_id('subjectsearchLogo').click()
browser.find_element_by_partial_link_text('高级搜索').click()
time.sleep(2)
# 切换iframe进去
browser.switch_to.frame(browser.find_element_by_id("advsearch_QMDialog__dlgiframe_"))
# 获得高级查询关键词
browser.find_element_by_id('keyword').send_keys('发票')
# 获得位置下拉框
browser.find_elements_by_class_name('ico_select_s')[0].click()
# 获得位置参数，这里标定为全部内容，根据自己选择
    # 主题参数：select_QMMenu__menuitem_2
    # 邮件正文参数：select_QMMenu__menuitem_1
    # 邮件名：select_QMMenu__menuitem_3
    # 全部内容：select_QMMenu__menuitem_0
browser.find_element_by_id('select_QMMenu__menuitem_0').click()
# 发件人，这里测试标定为滴滴，didiinvoice,这里默认是空，建议不进行设置，除非有特殊情况
# browser.find_element_by_id('sender').send_keys('didifapiao')
# 收件人，这里肯定是自己，默认空，可以不进行设置
# browser.find_element_by_id('receiver').send_keys('1296100904')
# 获得时间参数,这里默认选择为自定义
browser.find_elements_by_class_name('ico_select_s')[1].click()
# 使用下面的参数时
    # 不限：select_QMMenu__menuitem_st_
    # 一天内：select_QMMenu__menuitem_st_1
    # 三天内：select_QMMenu__menuitem_st_2
    # 一周内：select_QMMenu__menuitem_st_3
    # 一月内：select_QMMenu__menuitem_st_5
    # 一年内：select_QMMenu__menuitem_st_8
    # 自定义：select_QMMenu__menuitem_st_9
browser.find_element_by_id('select_QMMenu__menuitem_st_9').click()
# 书接上文，因为选择为自定义，这里需要手动指定开始日期
browser.execute_script("document.getElementById('starttimeinput').value='2019年3月4日'")
# 书接上文，因为选择为自定义，这里需要手动指定接数日期
browser.execute_script("document.getElementById('endtimeinput').value='2020年4月1日'")
# 获得文件夹参数,这里默认选择为收件箱
# browser.find_elements_by_class_name('ico_select_s')[2].click()
# 获取不同位置的参数，这里需要修改
    # 全部邮件：select_QMMenu__menuitem_all
    # 收件箱：select_QMMenu__menuitem_1
    # 草稿箱：select_QMMenu__menuitem_4
    # 已发送：select_QMMenu__menuitem_3
    # 群邮件：select_QMMenu__menuitem_8
    # 邮件归档：select_QMMenu__menuitem_130
# browser.find_element_by_id('select_QMMenu__menuitem_1').click()
# 获得是否包含附件参数,这里默认选择为包含附件，因为我们要的就是附件
browser.find_elements_by_class_name('ico_select_s')[3].click()
# 是否包含附件这里写死为包含附件
browser.find_element_by_id('select_QMMenu__menuitem_attach').click()
# 获得是否已读/未读参数,这里默认选择为不限制
browser.find_elements_by_class_name('ico_select_s')[4].click()
browser.find_element_by_id('select_QMMenu__menuitem_').click()
# 完成搜索参数设置，执行查询
browser.find_element_by_id('gosearch').click()
# 回到默认frame,进入新frame
browser.switch_to.default_content()
browser.switch_to.frame('mainFrame')
# 获得查询后的总条数
bd=browser.find_element_by_xpath('//*[@id="qqmail_mailcontainer"]/div[1]/div[1]/b')
# 这里需要判断是否进行了分页
bl=browser.find_element_by_xpath('//*[@id="frm"]/div[1]/div[1]')
bl_res=str(bl.text).replace(' ','').replace('页','/').split('/')
# 这里判断是否存在分页的情况,不存在分页意味着只有一页，那就很好说
if int(bl_res[0]) == int(bl_res[1]):
    for i in range(0, int(bd.text)):
        try:
            bf = browser.find_element_by_xpath('//*[@id="div_showbefore"]/table[%s+1]/tbody/tr/td[2]/div[2]' % i)
        except NoSuchElementException as e:
            print('洗内，采集完毕！%s'%e.args)
            browser.switch_to.window(browser.window_handles[0])
            browser.close()
        bf.click()
        # 在新页面进入新的frane并获取发件人信息
        browser.switch_to.window(browser.window_handles[-1])
        browser.switch_to.frame('mainFrame')
        addr = browser.find_element_by_id('tipFromAddr_readmail').get_attribute('fromaddr')
        # 切换到C盘下
        os.chdir('C://QQmail')
        # 判断是否创建过文件夹，没有就创建,有就进入//*[@id="attachment"]/div[2]/div[1]/a[1]  //*[@id="attachment"]/div[2]/div[1]/a[1]
        dir_list = os.listdir()
        if addr in dir_list:
            os.chdir('C://QQmail/%s' % addr)
            # 当再次执行，这里会造成直接进入，然后得获取时间
            time_text = browser.find_element_by_id('local-time-caption')
            time_perfix = str(time_text.text).split('（')[0]
            time_suffix = str(time_text.text).split('）')[1].replace(':', '时').replace(' ', '')
            mkdir_path = time_perfix + time_suffix
            os.mkdir(mkdir_path)
            os.chdir('%s/' % mkdir_path)
            now_cwd = os.getcwd()
            # 祖传代码，勿动
            chrome_options = Options()
            prefs = {"download.default_directory": now_cwd, "download.prompt_for_download": False, }
            chrome_options.add_experimental_option("prefs", prefs)
            browser.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
            params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': now_cwd}}
            command_result = browser.execute("send_command", params)
            # 祖传代码，勿动
            try:
                ba_obj = browser.find_element_by_id('attachmentCount')
                if int(ba_obj.text) > 1:
                    browser.find_element_by_xpath('//*[@id="attachment"]/div[2]/div[1]/a[1]').click()
                else:
                    browser.find_element_by_xpath('//*[@id="attachment"]/div[2]/div[2]/div[2]/div/a[2]').click()
            except NoSuchElementException as e:
                # 这里报错意味着没有附件
                pass
            # 关闭当前页面，防止浏览器崩溃
            time.sleep(5)
            browser.close()
        else:
            os.mkdir(addr)
            os.chdir('C://QQmail/%s' % addr)
            # 进来应该找收件时间，然后创建文件夹，进入文件夹，保存文件
            time_text = browser.find_element_by_id('local-time-caption')
            time_perfix = str(time_text.text).split('（')[0]
            time_suffix = str(time_text.text).split('）')[1].replace(':', '时').replace(' ', '')
            mkdir_path = time_perfix + time_suffix
            os.mkdir(mkdir_path)
            os.chdir('%s/' %mkdir_path)
            now_cwd = os.getcwd()
            # 祖传代码，勿动
            chrome_options = Options()
            prefs = {"download.default_directory": now_cwd, "download.prompt_for_download": False, }
            chrome_options.add_experimental_option("prefs", prefs)
            browser.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
            params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': now_cwd}}
            command_result = browser.execute("send_command", params)
            # 祖传代码，勿动
            try:
                ba_obj = browser.find_element_by_id('attachmentCount')
                if int(ba_obj.text) > 1:
                    browser.find_element_by_xpath('//*[@id="attachment"]/div[2]/div[1]/a[1]').click()
                else:
                    browser.find_element_by_xpath('//*[@id="attachment"]/div[2]/div[2]/div[2]/div/a[2]').click()
            except NoSuchElementException as e:
                # 这里报错意味着没有附件
                pass
            time.sleep(5)
            browser.close()
        browser.switch_to.window(browser.window_handles[0])
        browser.switch_to.default_content()
        browser.switch_to.frame('mainFrame')
else:
    # 点击勾选当前页，获得当前页数量
    browser.find_element_by_xpath('//*[@id="ckb_selectAll"]').click()
    li_obj=browser.find_element_by_xpath('//*[@id="div_mail_info"]')
    now_count=int(str(li_obj.text).split('页')[1].split('封')[0].replace(' ',''))
    bi = 1
    while bi <= int(bl_res[1]):
        for i in range(0, now_count):
            try:
                bf = browser.find_element_by_xpath('//*[@id="div_showbefore"]/table[%s+1]/tbody/tr/td[2]/div[2]' % i)
            except NoSuchElementException as e:
                print('喵内噶，采集完毕！%s'%e.args)
                browser.switch_to.window(browser.window_handles[0])
                browser.close()
            bf.click()
            # 在新页面进入新的frane并获取发件人信息
            browser.switch_to.window(browser.window_handles[-1])
            browser.switch_to.frame('mainFrame')
            addr = browser.find_element_by_id('tipFromAddr_readmail').get_attribute('fromaddr')
            # 切换到C盘下
            os.chdir('C://QQmail')
            # 判断是否创建过文件夹，没有就创建,有就进入//*[@id="attachment"]/div[2]/div[1]/a[1]  //*[@id="attachment"]/div[2]/div[1]/a[1]
            dir_list = os.listdir()
            if addr in dir_list:
                os.chdir('C://QQmail/%s' % addr)
                # 当再次执行，这里会造成直接进入，然后得获取时间
                time_text = browser.find_element_by_id('local-time-caption')
                time_perfix = str(time_text.text).split('（')[0]
                time_suffix = str(time_text.text).split('）')[1].replace(':', '时').replace(' ', '')
                mkdir_path = time_perfix + time_suffix
                os.mkdir(mkdir_path)
                os.chdir('%s/' % mkdir_path)
                now_cwd = os.getcwd()
                # 祖传代码，勿动
                chrome_options = Options()
                prefs = {"download.default_directory": now_cwd, "download.prompt_for_download": False, }
                chrome_options.add_experimental_option("prefs", prefs)
                browser.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
                params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': now_cwd}}
                command_result = browser.execute("send_command", params)
                # 祖传代码，勿动
                # 操，这里需要判断附件的个数，两个及两个以上才有全部下载按钮
                try:
                    ba_obj = browser.find_element_by_id('attachmentCount')
                    if int(ba_obj.text) > 1:
                        browser.find_element_by_xpath('//*[@id="attachment"]/div[2]/div[1]/a[1]').click()
                    else:
                        browser.find_element_by_xpath('//*[@id="attachment"]/div[2]/div[2]/div[2]/div/a[2]').click()
                except NoSuchElementException as e:
                    # 这里报错意味着没有附件
                    pass
                # 关闭当前页面，防止浏览器崩溃
                time.sleep(5)
                browser.close()
            else:
                os.mkdir(addr)
                os.chdir('C://QQmail/%s' % addr)
                # 进来应该找收件时间，然后创建文件夹，进入文件夹，保存文件
                time_text = browser.find_element_by_id('local-time-caption')
                time_perfix = str(time_text.text).split('（')[0]
                time_suffix = str(time_text.text).split('）')[1].replace(':', '时').replace(' ', '')
                mkdir_path = time_perfix + time_suffix
                os.mkdir(mkdir_path)
                os.chdir('%s/' % mkdir_path)
                now_cwd = os.getcwd()
                # 祖传代码，勿动
                chrome_options = Options()
                prefs = {"download.default_directory": now_cwd, "download.prompt_for_download": False, }
                chrome_options.add_experimental_option("prefs", prefs)
                browser.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
                params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': now_cwd}}
                command_result = browser.execute("send_command", params)
                # 祖传代码，勿动
                try:
                    ba_obj = browser.find_element_by_id('attachmentCount')
                    if int(ba_obj.text) >1:
                        browser.find_element_by_xpath('//*[@id="attachment"]/div[2]/div[1]/a[1]').click()
                    else:
                        browser.find_element_by_xpath('//*[@id="attachment"]/div[2]/div[2]/div[2]/div/a[2]').click()
                except NoSuchElementException as e:
                    # 这里报错意味着没有附件
                    pass
                time.sleep(5)
                browser.close()
            browser.switch_to.window(browser.window_handles[0])
            browser.switch_to.default_content()
            browser.switch_to.frame('mainFrame')
            # 循环脚本执行完毕，证明当前页采集已经完成,点击进入下一页
        browser.find_element_by_xpath('//*[@id="nextpage1"]').click()
        bi = int(bl_res[0]) + 1
